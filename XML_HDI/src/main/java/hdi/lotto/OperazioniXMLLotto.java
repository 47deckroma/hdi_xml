package hdi.lotto;

public class OperazioniXMLLotto {
	//OperazioniXMLLotto.getCustomerEnvelopID
	public static String getCustomerEnvelopeID(hdi.lotto.Lotto.Documento  valueObject,String TipoAmbiente) {
		return valueObject.getContratto().getNumeroPolizza()+"_"+ valueObject.getContratto().getPuntoVendita().getCodice()+"_"+valueObject.getContratto().getIdMovimento()+"_"+valueObject.getContratto().getCausale().getCodice()+"_"+valueObject.getTestata().getCodiceDocumento()+"_"+valueObject.getTestata().getTipologiaDms()+"_"+TipoAmbiente;
	}
	
	public static String getCustomerSetID(hdi.lotto.Lotto  valueObject ) {
	    	return		valueObject.getTemplate()+"_"+ valueObject.getPeriodoRiferimento()+"_"+valueObject.getIdentificativoLotto()  ;

	}
	
}
