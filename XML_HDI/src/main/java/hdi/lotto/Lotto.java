//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.02.01 alle 06:01:33 PM CET 
//


package hdi.lotto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificativo-lotto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="periodo-riferimento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="template" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="versione-template" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="servizio">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="mittente">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="codice-documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipologia-dms" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documento" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="testata">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="template" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="versione-template" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="servizio">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="mittente">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="codice-documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="tipologia-dms" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numero-copie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="destinatari">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="soggetto" maxOccurs="unbounded">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="recapito">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="nazione">
 *                                                             &lt;complexType>
 *                                                               &lt;simpleContent>
 *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                                                   &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/extension>
 *                                                               &lt;/simpleContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="modalita-invio">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="contratto">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="numero-polizza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="id-movimento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="causale">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="data-scadenza" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="contraente">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="soggetto">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="residenza">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="nazione">
 *                                                             &lt;complexType>
 *                                                               &lt;simpleContent>
 *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                                                   &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/extension>
 *                                                               &lt;/simpleContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="proprietario-veicolo">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="soggetto">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="residenza">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="nazione">
 *                                                             &lt;complexType>
 *                                                               &lt;simpleContent>
 *                                                                 &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                                                   &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/extension>
 *                                                               &lt;/simpleContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="beni">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="autovettura">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="targa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                 &lt;element name="rischi">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                   &lt;/sequence>
 *                                                                   &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="punto-vendita">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="canale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="size-lotto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "identificativoLotto",
    "periodoRiferimento",
    "template",
    "versioneTemplate",
    "servizio",
    "codiceDocumento",
    "tipologiaDms",
    "documento",
    "sizeLotto"
})
@XmlRootElement(name = "lotto")
public class Lotto {

    @XmlElement(name = "identificativo-lotto", required = true)
    protected String identificativoLotto;
    @XmlElement(name = "periodo-riferimento", required = true)
    protected String periodoRiferimento;
    @XmlElement(required = true)
    protected String template;
    @XmlElement(name = "versione-template", required = true)
    protected String versioneTemplate;
    @XmlElement(required = true)
    protected Lotto.Servizio servizio;
    @XmlElement(name = "codice-documento", required = true)
    protected String codiceDocumento;
    @XmlElement(name = "tipologia-dms", required = true)
    protected String tipologiaDms;
    protected List<Lotto.Documento> documento;
    @XmlElement(name = "size-lotto", required = true)
    protected String sizeLotto;

    /**
     * Recupera il valore della proprietÓ identificativoLotto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoLotto() {
        return identificativoLotto;
    }

    /**
     * Imposta il valore della proprietÓ identificativoLotto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoLotto(String value) {
        this.identificativoLotto = value;
    }

    /**
     * Recupera il valore della proprietÓ periodoRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodoRiferimento() {
        return periodoRiferimento;
    }

    /**
     * Imposta il valore della proprietÓ periodoRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodoRiferimento(String value) {
        this.periodoRiferimento = value;
    }

    /**
     * Recupera il valore della proprietÓ template.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplate() {
        return template;
    }

    /**
     * Imposta il valore della proprietÓ template.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplate(String value) {
        this.template = value;
    }

    /**
     * Recupera il valore della proprietÓ versioneTemplate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneTemplate() {
        return versioneTemplate;
    }

    /**
     * Imposta il valore della proprietÓ versioneTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneTemplate(String value) {
        this.versioneTemplate = value;
    }

    /**
     * Recupera il valore della proprietÓ servizio.
     * 
     * @return
     *     possible object is
     *     {@link Lotto.Servizio }
     *     
     */
    public Lotto.Servizio getServizio() {
        return servizio;
    }

    /**
     * Imposta il valore della proprietÓ servizio.
     * 
     * @param value
     *     allowed object is
     *     {@link Lotto.Servizio }
     *     
     */
    public void setServizio(Lotto.Servizio value) {
        this.servizio = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceDocumento() {
        return codiceDocumento;
    }

    /**
     * Imposta il valore della proprietÓ codiceDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceDocumento(String value) {
        this.codiceDocumento = value;
    }

    /**
     * Recupera il valore della proprietÓ tipologiaDms.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipologiaDms() {
        return tipologiaDms;
    }

    /**
     * Imposta il valore della proprietÓ tipologiaDms.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipologiaDms(String value) {
        this.tipologiaDms = value;
    }

    /**
     * Gets the value of the documento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Lotto.Documento }
     * 
     * 
     */
    public List<Lotto.Documento> getDocumento() {
        if (documento == null) {
            documento = new ArrayList<Lotto.Documento>();
        }
        return this.documento;
    }

    /**
     * Recupera il valore della proprietÓ sizeLotto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSizeLotto() {
        return sizeLotto;
    }

    /**
     * Imposta il valore della proprietÓ sizeLotto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSizeLotto(String value) {
        this.sizeLotto = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="testata">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="template" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="versione-template" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="servizio">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="mittente">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="codice-documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="tipologia-dms" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="numero-copie" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="destinatari">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="soggetto" maxOccurs="unbounded">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="recapito">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="nazione">
     *                                                   &lt;complexType>
     *                                                     &lt;simpleContent>
     *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                                                         &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/extension>
     *                                                     &lt;/simpleContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="modalita-invio">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="contratto">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="numero-polizza" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="id-movimento" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="causale">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="data-scadenza" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="contraente">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="soggetto">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="residenza">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="nazione">
     *                                                   &lt;complexType>
     *                                                     &lt;simpleContent>
     *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                                                         &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/extension>
     *                                                     &lt;/simpleContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="proprietario-veicolo">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="soggetto">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="residenza">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="nazione">
     *                                                   &lt;complexType>
     *                                                     &lt;simpleContent>
     *                                                       &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                                                         &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/extension>
     *                                                     &lt;/simpleContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="beni">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="autovettura">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="targa" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="rischi">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                         &lt;/sequence>
     *                                                         &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="punto-vendita">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="canale" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "testata",
        "contratto"
    })
    public static class Documento {

        @XmlElement(required = true)
        protected Lotto.Documento.Testata testata;
        @XmlElement(required = true)
        protected Lotto.Documento.Contratto contratto;

        /**
         * Recupera il valore della proprietÓ testata.
         * 
         * @return
         *     possible object is
         *     {@link Lotto.Documento.Testata }
         *     
         */
        public Lotto.Documento.Testata getTestata() {
            return testata;
        }

        /**
         * Imposta il valore della proprietÓ testata.
         * 
         * @param value
         *     allowed object is
         *     {@link Lotto.Documento.Testata }
         *     
         */
        public void setTestata(Lotto.Documento.Testata value) {
            this.testata = value;
        }

        /**
         * Recupera il valore della proprietÓ contratto.
         * 
         * @return
         *     possible object is
         *     {@link Lotto.Documento.Contratto }
         *     
         */
        public Lotto.Documento.Contratto getContratto() {
            return contratto;
        }

        /**
         * Imposta il valore della proprietÓ contratto.
         * 
         * @param value
         *     allowed object is
         *     {@link Lotto.Documento.Contratto }
         *     
         */
        public void setContratto(Lotto.Documento.Contratto value) {
            this.contratto = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="numero-polizza" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="id-movimento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="causale">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="data-scadenza" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="contraente">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="soggetto">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="residenza">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="nazione">
         *                                         &lt;complexType>
         *                                           &lt;simpleContent>
         *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                                               &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/extension>
         *                                           &lt;/simpleContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="proprietario-veicolo">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="soggetto">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="residenza">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="nazione">
         *                                         &lt;complexType>
         *                                           &lt;simpleContent>
         *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                                               &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/extension>
         *                                           &lt;/simpleContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="beni">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="autovettura">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="targa" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="rischi">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                               &lt;/sequence>
         *                                               &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="punto-vendita">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="canale" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "numeroPolizza",
            "idMovimento",
            "causale",
            "dataScadenza",
            "contraente",
            "proprietarioVeicolo",
            "beni",
            "puntoVendita"
        })
        public static class Contratto {

            @XmlElement(name = "numero-polizza", required = true)
            protected String numeroPolizza;
            @XmlElement(name = "id-movimento", required = true)
            protected String idMovimento;
            @XmlElement(required = true)
            protected Lotto.Documento.Contratto.Causale causale;
            @XmlElement(name = "data-scadenza", required = true)
            protected String dataScadenza;
            @XmlElement(required = true)
            protected Lotto.Documento.Contratto.Contraente contraente;
            @XmlElement(name = "proprietario-veicolo", required = true)
            protected Lotto.Documento.Contratto.ProprietarioVeicolo proprietarioVeicolo;
            @XmlElement(required = true)
            protected Lotto.Documento.Contratto.Beni beni;
            @XmlElement(name = "punto-vendita", required = true)
            protected Lotto.Documento.Contratto.PuntoVendita puntoVendita;

            /**
             * Recupera il valore della proprietÓ numeroPolizza.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumeroPolizza() {
                return numeroPolizza;
            }

            /**
             * Imposta il valore della proprietÓ numeroPolizza.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumeroPolizza(String value) {
                this.numeroPolizza = value;
            }

            /**
             * Recupera il valore della proprietÓ idMovimento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIdMovimento() {
                return idMovimento;
            }

            /**
             * Imposta il valore della proprietÓ idMovimento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIdMovimento(String value) {
                this.idMovimento = value;
            }

            /**
             * Recupera il valore della proprietÓ causale.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Contratto.Causale }
             *     
             */
            public Lotto.Documento.Contratto.Causale getCausale() {
                return causale;
            }

            /**
             * Imposta il valore della proprietÓ causale.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Contratto.Causale }
             *     
             */
            public void setCausale(Lotto.Documento.Contratto.Causale value) {
                this.causale = value;
            }

            /**
             * Recupera il valore della proprietÓ dataScadenza.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDataScadenza() {
                return dataScadenza;
            }

            /**
             * Imposta il valore della proprietÓ dataScadenza.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDataScadenza(String value) {
                this.dataScadenza = value;
            }

            /**
             * Recupera il valore della proprietÓ contraente.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Contratto.Contraente }
             *     
             */
            public Lotto.Documento.Contratto.Contraente getContraente() {
                return contraente;
            }

            /**
             * Imposta il valore della proprietÓ contraente.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Contratto.Contraente }
             *     
             */
            public void setContraente(Lotto.Documento.Contratto.Contraente value) {
                this.contraente = value;
            }

            /**
             * Recupera il valore della proprietÓ proprietarioVeicolo.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo }
             *     
             */
            public Lotto.Documento.Contratto.ProprietarioVeicolo getProprietarioVeicolo() {
                return proprietarioVeicolo;
            }

            /**
             * Imposta il valore della proprietÓ proprietarioVeicolo.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo }
             *     
             */
            public void setProprietarioVeicolo(Lotto.Documento.Contratto.ProprietarioVeicolo value) {
                this.proprietarioVeicolo = value;
            }

            /**
             * Recupera il valore della proprietÓ beni.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Contratto.Beni }
             *     
             */
            public Lotto.Documento.Contratto.Beni getBeni() {
                return beni;
            }

            /**
             * Imposta il valore della proprietÓ beni.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Contratto.Beni }
             *     
             */
            public void setBeni(Lotto.Documento.Contratto.Beni value) {
                this.beni = value;
            }

            /**
             * Recupera il valore della proprietÓ puntoVendita.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Contratto.PuntoVendita }
             *     
             */
            public Lotto.Documento.Contratto.PuntoVendita getPuntoVendita() {
                return puntoVendita;
            }

            /**
             * Imposta il valore della proprietÓ puntoVendita.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Contratto.PuntoVendita }
             *     
             */
            public void setPuntoVendita(Lotto.Documento.Contratto.PuntoVendita value) {
                this.puntoVendita = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="autovettura">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="targa" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="rischi">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                     &lt;/sequence>
             *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "autovettura"
            })
            public static class Beni {

                @XmlElement(required = true)
                protected Lotto.Documento.Contratto.Beni.Autovettura autovettura;

                /**
                 * Recupera il valore della proprietÓ autovettura.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Lotto.Documento.Contratto.Beni.Autovettura }
                 *     
                 */
                public Lotto.Documento.Contratto.Beni.Autovettura getAutovettura() {
                    return autovettura;
                }

                /**
                 * Imposta il valore della proprietÓ autovettura.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Lotto.Documento.Contratto.Beni.Autovettura }
                 *     
                 */
                public void setAutovettura(Lotto.Documento.Contratto.Beni.Autovettura value) {
                    this.autovettura = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="targa" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="rischi">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                           &lt;/sequence>
                 *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "targa",
                    "rischi"
                })
                public static class Autovettura {

                    @XmlElement(required = true)
                    protected String targa;
                    @XmlElement(required = true)
                    protected Lotto.Documento.Contratto.Beni.Autovettura.Rischi rischi;

                    /**
                     * Recupera il valore della proprietÓ targa.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTarga() {
                        return targa;
                    }

                    /**
                     * Imposta il valore della proprietÓ targa.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTarga(String value) {
                        this.targa = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ rischi.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Lotto.Documento.Contratto.Beni.Autovettura.Rischi }
                     *     
                     */
                    public Lotto.Documento.Contratto.Beni.Autovettura.Rischi getRischi() {
                        return rischi;
                    }

                    /**
                     * Imposta il valore della proprietÓ rischi.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Lotto.Documento.Contratto.Beni.Autovettura.Rischi }
                     *     
                     */
                    public void setRischi(Lotto.Documento.Contratto.Beni.Autovettura.Rischi value) {
                        this.rischi = value;
                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="rischio" maxOccurs="unbounded" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                 &lt;/sequence>
                     *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "rischio"
                    })
                    public static class Rischi {

                        protected List<Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio> rischio;

                        /**
                         * Gets the value of the rischio property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the rischio property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getRischio().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio }
                         * 
                         * 
                         */
                        public List<Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio> getRischio() {
                            if (rischio == null) {
                                rischio = new ArrayList<Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio>();
                            }
                            return this.rischio;
                        }


                        /**
                         * <p>Classe Java per anonymous complex type.
                         * 
                         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *       &lt;/sequence>
                         *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "descrizione"
                        })
                        public static class Rischio {

                            @XmlElement(required = true)
                            protected String descrizione;
                            @XmlAttribute(name = "codice")
                            protected String codice;

                            /**
                             * Recupera il valore della proprietÓ descrizione.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDescrizione() {
                                return descrizione;
                            }

                            /**
                             * Imposta il valore della proprietÓ descrizione.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDescrizione(String value) {
                                this.descrizione = value;
                            }

                            /**
                             * Recupera il valore della proprietÓ codice.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodice() {
                                return codice;
                            }

                            /**
                             * Imposta il valore della proprietÓ codice.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodice(String value) {
                                this.codice = value;
                            }

                        }

                    }

                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Causale {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "codice")
                protected String codice;

                /**
                 * Recupera il valore della proprietÓ value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * Imposta il valore della proprietÓ value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Recupera il valore della proprietÓ codice.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodice() {
                    return codice;
                }

                /**
                 * Imposta il valore della proprietÓ codice.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodice(String value) {
                    this.codice = value;
                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="soggetto">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="residenza">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="nazione">
             *                               &lt;complexType>
             *                                 &lt;simpleContent>
             *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/extension>
             *                                 &lt;/simpleContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "soggetto"
            })
            public static class Contraente {

                @XmlElement(required = true)
                protected Lotto.Documento.Contratto.Contraente.Soggetto soggetto;

                /**
                 * Recupera il valore della proprietÓ soggetto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Lotto.Documento.Contratto.Contraente.Soggetto }
                 *     
                 */
                public Lotto.Documento.Contratto.Contraente.Soggetto getSoggetto() {
                    return soggetto;
                }

                /**
                 * Imposta il valore della proprietÓ soggetto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Lotto.Documento.Contratto.Contraente.Soggetto }
                 *     
                 */
                public void setSoggetto(Lotto.Documento.Contratto.Contraente.Soggetto value) {
                    this.soggetto = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="residenza">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="nazione">
                 *                     &lt;complexType>
                 *                       &lt;simpleContent>
                 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                 *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/extension>
                 *                       &lt;/simpleContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nominativo",
                    "codiceFiscale",
                    "partitaIva",
                    "fisicoGiuridico",
                    "tipo",
                    "residenza",
                    "telefono",
                    "cellulare",
                    "email"
                })
                public static class Soggetto {

                    @XmlElement(required = true)
                    protected String nominativo;
                    @XmlElement(name = "codice-fiscale", required = true)
                    protected String codiceFiscale;
                    @XmlElement(name = "partita-iva", required = true)
                    protected String partitaIva;
                    @XmlElement(name = "fisico-giuridico", required = true)
                    protected String fisicoGiuridico;
                    @XmlElement(required = true)
                    protected String tipo;
                    @XmlElement(required = true)
                    protected Lotto.Documento.Contratto.Contraente.Soggetto.Residenza residenza;
                    @XmlElement(required = true)
                    protected String telefono;
                    @XmlElement(required = true)
                    protected String cellulare;
                    @XmlElement(required = true)
                    protected String email;

                    /**
                     * Recupera il valore della proprietÓ nominativo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNominativo() {
                        return nominativo;
                    }

                    /**
                     * Imposta il valore della proprietÓ nominativo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNominativo(String value) {
                        this.nominativo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ codiceFiscale.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodiceFiscale() {
                        return codiceFiscale;
                    }

                    /**
                     * Imposta il valore della proprietÓ codiceFiscale.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodiceFiscale(String value) {
                        this.codiceFiscale = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ partitaIva.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPartitaIva() {
                        return partitaIva;
                    }

                    /**
                     * Imposta il valore della proprietÓ partitaIva.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPartitaIva(String value) {
                        this.partitaIva = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ fisicoGiuridico.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFisicoGiuridico() {
                        return fisicoGiuridico;
                    }

                    /**
                     * Imposta il valore della proprietÓ fisicoGiuridico.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFisicoGiuridico(String value) {
                        this.fisicoGiuridico = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ tipo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTipo() {
                        return tipo;
                    }

                    /**
                     * Imposta il valore della proprietÓ tipo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTipo(String value) {
                        this.tipo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ residenza.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza }
                     *     
                     */
                    public Lotto.Documento.Contratto.Contraente.Soggetto.Residenza getResidenza() {
                        return residenza;
                    }

                    /**
                     * Imposta il valore della proprietÓ residenza.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza }
                     *     
                     */
                    public void setResidenza(Lotto.Documento.Contratto.Contraente.Soggetto.Residenza value) {
                        this.residenza = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ telefono.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTelefono() {
                        return telefono;
                    }

                    /**
                     * Imposta il valore della proprietÓ telefono.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTelefono(String value) {
                        this.telefono = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ cellulare.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCellulare() {
                        return cellulare;
                    }

                    /**
                     * Imposta il valore della proprietÓ cellulare.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCellulare(String value) {
                        this.cellulare = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ email.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEmail() {
                        return email;
                    }

                    /**
                     * Imposta il valore della proprietÓ email.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEmail(String value) {
                        this.email = value;
                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="nazione">
                     *           &lt;complexType>
                     *             &lt;simpleContent>
                     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                     *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/extension>
                     *             &lt;/simpleContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "toponimo",
                        "indirizzo",
                        "numeroCivico",
                        "cap",
                        "comune",
                        "provincia",
                        "siglaProvincia",
                        "nazione"
                    })
                    public static class Residenza {

                        @XmlElement(required = true)
                        protected String toponimo;
                        @XmlElement(required = true)
                        protected String indirizzo;
                        @XmlElement(name = "numero-civico", required = true)
                        protected String numeroCivico;
                        @XmlElement(required = true)
                        protected String cap;
                        @XmlElement(required = true)
                        protected String comune;
                        @XmlElement(required = true)
                        protected String provincia;
                        @XmlElement(name = "sigla-provincia", required = true)
                        protected String siglaProvincia;
                        @XmlElement(required = true)
                        protected Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione nazione;

                        /**
                         * Recupera il valore della proprietÓ toponimo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getToponimo() {
                            return toponimo;
                        }

                        /**
                         * Imposta il valore della proprietÓ toponimo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setToponimo(String value) {
                            this.toponimo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ indirizzo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getIndirizzo() {
                            return indirizzo;
                        }

                        /**
                         * Imposta il valore della proprietÓ indirizzo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIndirizzo(String value) {
                            this.indirizzo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ numeroCivico.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNumeroCivico() {
                            return numeroCivico;
                        }

                        /**
                         * Imposta il valore della proprietÓ numeroCivico.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNumeroCivico(String value) {
                            this.numeroCivico = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ cap.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCap() {
                            return cap;
                        }

                        /**
                         * Imposta il valore della proprietÓ cap.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCap(String value) {
                            this.cap = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ comune.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getComune() {
                            return comune;
                        }

                        /**
                         * Imposta il valore della proprietÓ comune.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setComune(String value) {
                            this.comune = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ provincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getProvincia() {
                            return provincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ provincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setProvincia(String value) {
                            this.provincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ siglaProvincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiglaProvincia() {
                            return siglaProvincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ siglaProvincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiglaProvincia(String value) {
                            this.siglaProvincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ nazione.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione }
                         *     
                         */
                        public Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione getNazione() {
                            return nazione;
                        }

                        /**
                         * Imposta il valore della proprietÓ nazione.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione }
                         *     
                         */
                        public void setNazione(Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione value) {
                            this.nazione = value;
                        }


                        /**
                         * <p>Classe Java per anonymous complex type.
                         * 
                         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;simpleContent>
                         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                         *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/extension>
                         *   &lt;/simpleContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "value"
                        })
                        public static class Nazione {

                            @XmlValue
                            protected String value;
                            @XmlAttribute(name = "codice")
                            protected String codice;

                            /**
                             * Recupera il valore della proprietÓ value.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getValue() {
                                return value;
                            }

                            /**
                             * Imposta il valore della proprietÓ value.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setValue(String value) {
                                this.value = value;
                            }

                            /**
                             * Recupera il valore della proprietÓ codice.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodice() {
                                return codice;
                            }

                            /**
                             * Imposta il valore della proprietÓ codice.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodice(String value) {
                                this.codice = value;
                            }

                        }

                    }

                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="soggetto">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="residenza">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="nazione">
             *                               &lt;complexType>
             *                                 &lt;simpleContent>
             *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/extension>
             *                                 &lt;/simpleContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "soggetto"
            })
            public static class ProprietarioVeicolo {

                @XmlElement(required = true)
                protected Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto soggetto;

                /**
                 * Recupera il valore della proprietÓ soggetto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto }
                 *     
                 */
                public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto getSoggetto() {
                    return soggetto;
                }

                /**
                 * Imposta il valore della proprietÓ soggetto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto }
                 *     
                 */
                public void setSoggetto(Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto value) {
                    this.soggetto = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="codice-fiscale" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="partita-iva" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="fisico-giuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="residenza">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="nazione">
                 *                     &lt;complexType>
                 *                       &lt;simpleContent>
                 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                 *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/extension>
                 *                       &lt;/simpleContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nominativo",
                    "codiceFiscale",
                    "partitaIva",
                    "fisicoGiuridico",
                    "tipo",
                    "residenza",
                    "telefono",
                    "cellulare",
                    "email"
                })
                public static class Soggetto {

                    @XmlElement(required = true)
                    protected String nominativo;
                    @XmlElement(name = "codice-fiscale", required = true)
                    protected String codiceFiscale;
                    @XmlElement(name = "partita-iva", required = true)
                    protected String partitaIva;
                    @XmlElement(name = "fisico-giuridico", required = true)
                    protected String fisicoGiuridico;
                    @XmlElement(required = true)
                    protected String tipo;
                    @XmlElement(required = true)
                    protected Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza residenza;
                    @XmlElement(required = true)
                    protected String telefono;
                    @XmlElement(required = true)
                    protected String cellulare;
                    @XmlElement(required = true)
                    protected String email;

                    /**
                     * Recupera il valore della proprietÓ nominativo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNominativo() {
                        return nominativo;
                    }

                    /**
                     * Imposta il valore della proprietÓ nominativo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNominativo(String value) {
                        this.nominativo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ codiceFiscale.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCodiceFiscale() {
                        return codiceFiscale;
                    }

                    /**
                     * Imposta il valore della proprietÓ codiceFiscale.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCodiceFiscale(String value) {
                        this.codiceFiscale = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ partitaIva.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPartitaIva() {
                        return partitaIva;
                    }

                    /**
                     * Imposta il valore della proprietÓ partitaIva.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPartitaIva(String value) {
                        this.partitaIva = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ fisicoGiuridico.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFisicoGiuridico() {
                        return fisicoGiuridico;
                    }

                    /**
                     * Imposta il valore della proprietÓ fisicoGiuridico.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFisicoGiuridico(String value) {
                        this.fisicoGiuridico = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ tipo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTipo() {
                        return tipo;
                    }

                    /**
                     * Imposta il valore della proprietÓ tipo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTipo(String value) {
                        this.tipo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ residenza.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza }
                     *     
                     */
                    public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza getResidenza() {
                        return residenza;
                    }

                    /**
                     * Imposta il valore della proprietÓ residenza.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza }
                     *     
                     */
                    public void setResidenza(Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza value) {
                        this.residenza = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ telefono.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTelefono() {
                        return telefono;
                    }

                    /**
                     * Imposta il valore della proprietÓ telefono.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTelefono(String value) {
                        this.telefono = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ cellulare.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCellulare() {
                        return cellulare;
                    }

                    /**
                     * Imposta il valore della proprietÓ cellulare.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCellulare(String value) {
                        this.cellulare = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ email.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEmail() {
                        return email;
                    }

                    /**
                     * Imposta il valore della proprietÓ email.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEmail(String value) {
                        this.email = value;
                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="nazione">
                     *           &lt;complexType>
                     *             &lt;simpleContent>
                     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                     *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/extension>
                     *             &lt;/simpleContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "toponimo",
                        "indirizzo",
                        "numeroCivico",
                        "cap",
                        "comune",
                        "provincia",
                        "siglaProvincia",
                        "nazione"
                    })
                    public static class Residenza {

                        @XmlElement(required = true)
                        protected String toponimo;
                        @XmlElement(required = true)
                        protected String indirizzo;
                        @XmlElement(name = "numero-civico", required = true)
                        protected String numeroCivico;
                        @XmlElement(required = true)
                        protected String cap;
                        @XmlElement(required = true)
                        protected String comune;
                        @XmlElement(required = true)
                        protected String provincia;
                        @XmlElement(name = "sigla-provincia", required = true)
                        protected String siglaProvincia;
                        @XmlElement(required = true)
                        protected Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione nazione;

                        /**
                         * Recupera il valore della proprietÓ toponimo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getToponimo() {
                            return toponimo;
                        }

                        /**
                         * Imposta il valore della proprietÓ toponimo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setToponimo(String value) {
                            this.toponimo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ indirizzo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getIndirizzo() {
                            return indirizzo;
                        }

                        /**
                         * Imposta il valore della proprietÓ indirizzo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIndirizzo(String value) {
                            this.indirizzo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ numeroCivico.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNumeroCivico() {
                            return numeroCivico;
                        }

                        /**
                         * Imposta il valore della proprietÓ numeroCivico.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNumeroCivico(String value) {
                            this.numeroCivico = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ cap.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCap() {
                            return cap;
                        }

                        /**
                         * Imposta il valore della proprietÓ cap.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCap(String value) {
                            this.cap = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ comune.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getComune() {
                            return comune;
                        }

                        /**
                         * Imposta il valore della proprietÓ comune.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setComune(String value) {
                            this.comune = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ provincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getProvincia() {
                            return provincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ provincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setProvincia(String value) {
                            this.provincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ siglaProvincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiglaProvincia() {
                            return siglaProvincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ siglaProvincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiglaProvincia(String value) {
                            this.siglaProvincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ nazione.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione }
                         *     
                         */
                        public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione getNazione() {
                            return nazione;
                        }

                        /**
                         * Imposta il valore della proprietÓ nazione.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione }
                         *     
                         */
                        public void setNazione(Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione value) {
                            this.nazione = value;
                        }


                        /**
                         * <p>Classe Java per anonymous complex type.
                         * 
                         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;simpleContent>
                         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                         *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/extension>
                         *   &lt;/simpleContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "value"
                        })
                        public static class Nazione {

                            @XmlValue
                            protected String value;
                            @XmlAttribute(name = "codice")
                            protected String codice;

                            /**
                             * Recupera il valore della proprietÓ value.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getValue() {
                                return value;
                            }

                            /**
                             * Imposta il valore della proprietÓ value.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setValue(String value) {
                                this.value = value;
                            }

                            /**
                             * Recupera il valore della proprietÓ codice.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodice() {
                                return codice;
                            }

                            /**
                             * Imposta il valore della proprietÓ codice.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodice(String value) {
                                this.codice = value;
                            }

                        }

                    }

                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="canale" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "denominazione",
                "canale",
                "indirizzo",
                "cap",
                "comune",
                "provincia",
                "telefono",
                "fax",
                "email"
            })
            public static class PuntoVendita {

                @XmlElement(required = true)
                protected String denominazione;
                @XmlElement(required = true)
                protected String canale;
                @XmlElement(required = true)
                protected String indirizzo;
                @XmlElement(required = true)
                protected String cap;
                @XmlElement(required = true)
                protected String comune;
                @XmlElement(required = true)
                protected String provincia;
                @XmlElement(required = true)
                protected String telefono;
                @XmlElement(required = true)
                protected String fax;
                @XmlElement(required = true)
                protected String email;
                @XmlAttribute(name = "codice")
                protected String codice;

                /**
                 * Recupera il valore della proprietÓ denominazione.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDenominazione() {
                    return denominazione;
                }

                /**
                 * Imposta il valore della proprietÓ denominazione.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDenominazione(String value) {
                    this.denominazione = value;
                }

                /**
                 * Recupera il valore della proprietÓ canale.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCanale() {
                    return canale;
                }

                /**
                 * Imposta il valore della proprietÓ canale.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCanale(String value) {
                    this.canale = value;
                }

                /**
                 * Recupera il valore della proprietÓ indirizzo.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIndirizzo() {
                    return indirizzo;
                }

                /**
                 * Imposta il valore della proprietÓ indirizzo.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIndirizzo(String value) {
                    this.indirizzo = value;
                }

                /**
                 * Recupera il valore della proprietÓ cap.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCap() {
                    return cap;
                }

                /**
                 * Imposta il valore della proprietÓ cap.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCap(String value) {
                    this.cap = value;
                }

                /**
                 * Recupera il valore della proprietÓ comune.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getComune() {
                    return comune;
                }

                /**
                 * Imposta il valore della proprietÓ comune.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setComune(String value) {
                    this.comune = value;
                }

                /**
                 * Recupera il valore della proprietÓ provincia.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProvincia() {
                    return provincia;
                }

                /**
                 * Imposta il valore della proprietÓ provincia.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProvincia(String value) {
                    this.provincia = value;
                }

                /**
                 * Recupera il valore della proprietÓ telefono.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTelefono() {
                    return telefono;
                }

                /**
                 * Imposta il valore della proprietÓ telefono.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTelefono(String value) {
                    this.telefono = value;
                }

                /**
                 * Recupera il valore della proprietÓ fax.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFax() {
                    return fax;
                }

                /**
                 * Imposta il valore della proprietÓ fax.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFax(String value) {
                    this.fax = value;
                }

                /**
                 * Recupera il valore della proprietÓ email.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEmail() {
                    return email;
                }

                /**
                 * Imposta il valore della proprietÓ email.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEmail(String value) {
                    this.email = value;
                }

                /**
                 * Recupera il valore della proprietÓ codice.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodice() {
                    return codice;
                }

                /**
                 * Imposta il valore della proprietÓ codice.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodice(String value) {
                    this.codice = value;
                }

            }

        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="template" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="versione-template" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="servizio">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="mittente">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="codice-documento" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="tipologia-dms" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="numero-copie" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="destinatari">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="soggetto" maxOccurs="unbounded">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="recapito">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="nazione">
         *                                         &lt;complexType>
         *                                           &lt;simpleContent>
         *                                             &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                                               &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/extension>
         *                                           &lt;/simpleContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="modalita-invio">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "template",
            "versioneTemplate",
            "servizio",
            "codiceDocumento",
            "tipologiaDms",
            "numeroCopie",
            "destinatari"
        })
        public static class Testata {

            @XmlElement(required = true)
            protected String template;
            @XmlElement(name = "versione-template", required = true)
            protected String versioneTemplate;
            @XmlElement(required = true)
            protected Lotto.Documento.Testata.Servizio servizio;
            @XmlElement(name = "codice-documento", required = true)
            protected String codiceDocumento;
            @XmlElement(name = "tipologia-dms", required = true)
            protected String tipologiaDms;
            @XmlElement(name = "numero-copie", required = true)
            protected String numeroCopie;
            @XmlElement(required = true)
            protected Lotto.Documento.Testata.Destinatari destinatari;

            /**
             * Recupera il valore della proprietÓ template.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTemplate() {
                return template;
            }

            /**
             * Imposta il valore della proprietÓ template.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTemplate(String value) {
                this.template = value;
            }

            /**
             * Recupera il valore della proprietÓ versioneTemplate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVersioneTemplate() {
                return versioneTemplate;
            }

            /**
             * Imposta il valore della proprietÓ versioneTemplate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVersioneTemplate(String value) {
                this.versioneTemplate = value;
            }

            /**
             * Recupera il valore della proprietÓ servizio.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Testata.Servizio }
             *     
             */
            public Lotto.Documento.Testata.Servizio getServizio() {
                return servizio;
            }

            /**
             * Imposta il valore della proprietÓ servizio.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Testata.Servizio }
             *     
             */
            public void setServizio(Lotto.Documento.Testata.Servizio value) {
                this.servizio = value;
            }

            /**
             * Recupera il valore della proprietÓ codiceDocumento.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCodiceDocumento() {
                return codiceDocumento;
            }

            /**
             * Imposta il valore della proprietÓ codiceDocumento.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCodiceDocumento(String value) {
                this.codiceDocumento = value;
            }

            /**
             * Recupera il valore della proprietÓ tipologiaDms.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTipologiaDms() {
                return tipologiaDms;
            }

            /**
             * Imposta il valore della proprietÓ tipologiaDms.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTipologiaDms(String value) {
                this.tipologiaDms = value;
            }

            /**
             * Recupera il valore della proprietÓ numeroCopie.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNumeroCopie() {
                return numeroCopie;
            }

            /**
             * Imposta il valore della proprietÓ numeroCopie.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNumeroCopie(String value) {
                this.numeroCopie = value;
            }

            /**
             * Recupera il valore della proprietÓ destinatari.
             * 
             * @return
             *     possible object is
             *     {@link Lotto.Documento.Testata.Destinatari }
             *     
             */
            public Lotto.Documento.Testata.Destinatari getDestinatari() {
                return destinatari;
            }

            /**
             * Imposta il valore della proprietÓ destinatari.
             * 
             * @param value
             *     allowed object is
             *     {@link Lotto.Documento.Testata.Destinatari }
             *     
             */
            public void setDestinatari(Lotto.Documento.Testata.Destinatari value) {
                this.destinatari = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="soggetto" maxOccurs="unbounded">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="recapito">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="nazione">
             *                               &lt;complexType>
             *                                 &lt;simpleContent>
             *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *                                     &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/extension>
             *                                 &lt;/simpleContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="modalita-invio">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "soggetto"
            })
            public static class Destinatari {

                @XmlElement(required = true)
                protected List<Lotto.Documento.Testata.Destinatari.Soggetto> soggetto;

                /**
                 * Gets the value of the soggetto property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the soggetto property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getSoggetto().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Lotto.Documento.Testata.Destinatari.Soggetto }
                 * 
                 * 
                 */
                public List<Lotto.Documento.Testata.Destinatari.Soggetto> getSoggetto() {
                    if (soggetto == null) {
                        soggetto = new ArrayList<Lotto.Documento.Testata.Destinatari.Soggetto>();
                    }
                    return this.soggetto;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="nominativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="recapito">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="nazione">
                 *                     &lt;complexType>
                 *                       &lt;simpleContent>
                 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                 *                           &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/extension>
                 *                       &lt;/simpleContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="cellulare" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="modalita-invio">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "nominativo",
                    "recapito",
                    "cellulare",
                    "email",
                    "modalitaInvio"
                })
                public static class Soggetto {

                    @XmlElement(required = true)
                    protected String nominativo;
                    @XmlElement(required = true)
                    protected Lotto.Documento.Testata.Destinatari.Soggetto.Recapito recapito;
                    @XmlElement(required = true)
                    protected String cellulare;
                    @XmlElement(required = true)
                    protected String email;
                    @XmlElement(name = "modalita-invio", required = true)
                    protected Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio modalitaInvio;

                    /**
                     * Recupera il valore della proprietÓ nominativo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNominativo() {
                        return nominativo;
                    }

                    /**
                     * Imposta il valore della proprietÓ nominativo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNominativo(String value) {
                        this.nominativo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ recapito.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito }
                     *     
                     */
                    public Lotto.Documento.Testata.Destinatari.Soggetto.Recapito getRecapito() {
                        return recapito;
                    }

                    /**
                     * Imposta il valore della proprietÓ recapito.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito }
                     *     
                     */
                    public void setRecapito(Lotto.Documento.Testata.Destinatari.Soggetto.Recapito value) {
                        this.recapito = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ cellulare.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCellulare() {
                        return cellulare;
                    }

                    /**
                     * Imposta il valore della proprietÓ cellulare.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCellulare(String value) {
                        this.cellulare = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ email.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEmail() {
                        return email;
                    }

                    /**
                     * Imposta il valore della proprietÓ email.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEmail(String value) {
                        this.email = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ modalitaInvio.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio }
                     *     
                     */
                    public Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio getModalitaInvio() {
                        return modalitaInvio;
                    }

                    /**
                     * Imposta il valore della proprietÓ modalitaInvio.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio }
                     *     
                     */
                    public void setModalitaInvio(Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio value) {
                        this.modalitaInvio = value;
                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="sms" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "sms",
                        "email"
                    })
                    public static class ModalitaInvio {

                        @XmlElement(required = true)
                        protected String sms;
                        @XmlElement(required = true)
                        protected String email;

                        /**
                         * Recupera il valore della proprietÓ sms.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSms() {
                            return sms;
                        }

                        /**
                         * Imposta il valore della proprietÓ sms.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSms(String value) {
                            this.sms = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ email.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEmail() {
                            return email;
                        }

                        /**
                         * Imposta il valore della proprietÓ email.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEmail(String value) {
                            this.email = value;
                        }

                    }


                    /**
                     * <p>Classe Java per anonymous complex type.
                     * 
                     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="toponimo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="numero-civico" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="comune" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="sigla-provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="nazione">
                     *           &lt;complexType>
                     *             &lt;simpleContent>
                     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                     *                 &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/extension>
                     *             &lt;/simpleContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "toponimo",
                        "indirizzo",
                        "numeroCivico",
                        "cap",
                        "comune",
                        "provincia",
                        "siglaProvincia",
                        "nazione"
                    })
                    public static class Recapito {

                        @XmlElement(required = true)
                        protected String toponimo;
                        @XmlElement(required = true)
                        protected String indirizzo;
                        @XmlElement(name = "numero-civico", required = true)
                        protected String numeroCivico;
                        @XmlElement(required = true)
                        protected String cap;
                        @XmlElement(required = true)
                        protected String comune;
                        @XmlElement(required = true)
                        protected String provincia;
                        @XmlElement(name = "sigla-provincia", required = true)
                        protected String siglaProvincia;
                        @XmlElement(required = true)
                        protected Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione nazione;

                        /**
                         * Recupera il valore della proprietÓ toponimo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getToponimo() {
                            return toponimo;
                        }

                        /**
                         * Imposta il valore della proprietÓ toponimo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setToponimo(String value) {
                            this.toponimo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ indirizzo.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getIndirizzo() {
                            return indirizzo;
                        }

                        /**
                         * Imposta il valore della proprietÓ indirizzo.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIndirizzo(String value) {
                            this.indirizzo = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ numeroCivico.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNumeroCivico() {
                            return numeroCivico;
                        }

                        /**
                         * Imposta il valore della proprietÓ numeroCivico.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNumeroCivico(String value) {
                            this.numeroCivico = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ cap.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCap() {
                            return cap;
                        }

                        /**
                         * Imposta il valore della proprietÓ cap.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCap(String value) {
                            this.cap = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ comune.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getComune() {
                            return comune;
                        }

                        /**
                         * Imposta il valore della proprietÓ comune.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setComune(String value) {
                            this.comune = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ provincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getProvincia() {
                            return provincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ provincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setProvincia(String value) {
                            this.provincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ siglaProvincia.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiglaProvincia() {
                            return siglaProvincia;
                        }

                        /**
                         * Imposta il valore della proprietÓ siglaProvincia.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiglaProvincia(String value) {
                            this.siglaProvincia = value;
                        }

                        /**
                         * Recupera il valore della proprietÓ nazione.
                         * 
                         * @return
                         *     possible object is
                         *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione }
                         *     
                         */
                        public Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione getNazione() {
                            return nazione;
                        }

                        /**
                         * Imposta il valore della proprietÓ nazione.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione }
                         *     
                         */
                        public void setNazione(Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione value) {
                            this.nazione = value;
                        }


                        /**
                         * <p>Classe Java per anonymous complex type.
                         * 
                         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;simpleContent>
                         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
                         *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/extension>
                         *   &lt;/simpleContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "value"
                        })
                        public static class Nazione {

                            @XmlValue
                            protected String value;
                            @XmlAttribute(name = "codice")
                            protected String codice;

                            /**
                             * Recupera il valore della proprietÓ value.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getValue() {
                                return value;
                            }

                            /**
                             * Imposta il valore della proprietÓ value.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setValue(String value) {
                                this.value = value;
                            }

                            /**
                             * Recupera il valore della proprietÓ codice.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCodice() {
                                return codice;
                            }

                            /**
                             * Imposta il valore della proprietÓ codice.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCodice(String value) {
                                this.codice = value;
                            }

                        }

                    }

                }

            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="mittente">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "centroDiCosto",
                "tipoInvio",
                "mittente"
            })
            public static class Servizio {

                @XmlElement(name = "centro-di-costo", required = true)
                protected String centroDiCosto;
                @XmlElement(name = "tipo-invio", required = true)
                protected String tipoInvio;
                @XmlElement(required = true)
                protected Lotto.Documento.Testata.Servizio.Mittente mittente;
                @XmlAttribute(name = "codice")
                protected String codice;

                /**
                 * Recupera il valore della proprietÓ centroDiCosto.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCentroDiCosto() {
                    return centroDiCosto;
                }

                /**
                 * Imposta il valore della proprietÓ centroDiCosto.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCentroDiCosto(String value) {
                    this.centroDiCosto = value;
                }

                /**
                 * Recupera il valore della proprietÓ tipoInvio.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTipoInvio() {
                    return tipoInvio;
                }

                /**
                 * Imposta il valore della proprietÓ tipoInvio.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTipoInvio(String value) {
                    this.tipoInvio = value;
                }

                /**
                 * Recupera il valore della proprietÓ mittente.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Lotto.Documento.Testata.Servizio.Mittente }
                 *     
                 */
                public Lotto.Documento.Testata.Servizio.Mittente getMittente() {
                    return mittente;
                }

                /**
                 * Imposta il valore della proprietÓ mittente.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Lotto.Documento.Testata.Servizio.Mittente }
                 *     
                 */
                public void setMittente(Lotto.Documento.Testata.Servizio.Mittente value) {
                    this.mittente = value;
                }

                /**
                 * Recupera il valore della proprietÓ codice.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCodice() {
                    return codice;
                }

                /**
                 * Imposta il valore della proprietÓ codice.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCodice(String value) {
                    this.codice = value;
                }


                /**
                 * <p>Classe Java per anonymous complex type.
                 * 
                 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "ragioneSociale",
                    "indirizzo",
                    "citta",
                    "cap",
                    "provincia",
                    "nazione"
                })
                public static class Mittente {

                    @XmlElement(name = "ragione-sociale", required = true)
                    protected String ragioneSociale;
                    @XmlElement(required = true)
                    protected String indirizzo;
                    @XmlElement(required = true)
                    protected String citta;
                    @XmlElement(required = true)
                    protected String cap;
                    @XmlElement(required = true)
                    protected String provincia;
                    @XmlElement(required = true)
                    protected String nazione;

                    /**
                     * Recupera il valore della proprietÓ ragioneSociale.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getRagioneSociale() {
                        return ragioneSociale;
                    }

                    /**
                     * Imposta il valore della proprietÓ ragioneSociale.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setRagioneSociale(String value) {
                        this.ragioneSociale = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ indirizzo.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getIndirizzo() {
                        return indirizzo;
                    }

                    /**
                     * Imposta il valore della proprietÓ indirizzo.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIndirizzo(String value) {
                        this.indirizzo = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ citta.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCitta() {
                        return citta;
                    }

                    /**
                     * Imposta il valore della proprietÓ citta.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCitta(String value) {
                        this.citta = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ cap.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCap() {
                        return cap;
                    }

                    /**
                     * Imposta il valore della proprietÓ cap.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCap(String value) {
                        this.cap = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ provincia.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getProvincia() {
                        return provincia;
                    }

                    /**
                     * Imposta il valore della proprietÓ provincia.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setProvincia(String value) {
                        this.provincia = value;
                    }

                    /**
                     * Recupera il valore della proprietÓ nazione.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getNazione() {
                        return nazione;
                    }

                    /**
                     * Imposta il valore della proprietÓ nazione.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setNazione(String value) {
                        this.nazione = value;
                    }

                }

            }

        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="centro-di-costo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="tipo-invio" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="mittente">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="codice" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "centroDiCosto",
        "tipoInvio",
        "mittente"
    })
    public static class Servizio {

        @XmlElement(name = "centro-di-costo", required = true)
        protected String centroDiCosto;
        @XmlElement(name = "tipo-invio", required = true)
        protected String tipoInvio;
        @XmlElement(required = true)
        protected Lotto.Servizio.Mittente mittente;
        @XmlAttribute(name = "codice")
        protected String codice;

        /**
         * Recupera il valore della proprietÓ centroDiCosto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCentroDiCosto() {
            return centroDiCosto;
        }

        /**
         * Imposta il valore della proprietÓ centroDiCosto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCentroDiCosto(String value) {
            this.centroDiCosto = value;
        }

        /**
         * Recupera il valore della proprietÓ tipoInvio.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoInvio() {
            return tipoInvio;
        }

        /**
         * Imposta il valore della proprietÓ tipoInvio.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoInvio(String value) {
            this.tipoInvio = value;
        }

        /**
         * Recupera il valore della proprietÓ mittente.
         * 
         * @return
         *     possible object is
         *     {@link Lotto.Servizio.Mittente }
         *     
         */
        public Lotto.Servizio.Mittente getMittente() {
            return mittente;
        }

        /**
         * Imposta il valore della proprietÓ mittente.
         * 
         * @param value
         *     allowed object is
         *     {@link Lotto.Servizio.Mittente }
         *     
         */
        public void setMittente(Lotto.Servizio.Mittente value) {
            this.mittente = value;
        }

        /**
         * Recupera il valore della proprietÓ codice.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodice() {
            return codice;
        }

        /**
         * Imposta il valore della proprietÓ codice.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodice(String value) {
            this.codice = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ragione-sociale" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="citta" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cap" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="nazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ragioneSociale",
            "indirizzo",
            "citta",
            "cap",
            "provincia",
            "nazione"
        })
        public static class Mittente {

            @XmlElement(name = "ragione-sociale", required = true)
            protected String ragioneSociale;
            @XmlElement(required = true)
            protected String indirizzo;
            @XmlElement(required = true)
            protected String citta;
            @XmlElement(required = true)
            protected String cap;
            @XmlElement(required = true)
            protected String provincia;
            @XmlElement(required = true)
            protected String nazione;

            /**
             * Recupera il valore della proprietÓ ragioneSociale.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRagioneSociale() {
                return ragioneSociale;
            }

            /**
             * Imposta il valore della proprietÓ ragioneSociale.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRagioneSociale(String value) {
                this.ragioneSociale = value;
            }

            /**
             * Recupera il valore della proprietÓ indirizzo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getIndirizzo() {
                return indirizzo;
            }

            /**
             * Imposta il valore della proprietÓ indirizzo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setIndirizzo(String value) {
                this.indirizzo = value;
            }

            /**
             * Recupera il valore della proprietÓ citta.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCitta() {
                return citta;
            }

            /**
             * Imposta il valore della proprietÓ citta.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCitta(String value) {
                this.citta = value;
            }

            /**
             * Recupera il valore della proprietÓ cap.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCap() {
                return cap;
            }

            /**
             * Imposta il valore della proprietÓ cap.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCap(String value) {
                this.cap = value;
            }

            /**
             * Recupera il valore della proprietÓ provincia.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProvincia() {
                return provincia;
            }

            /**
             * Imposta il valore della proprietÓ provincia.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProvincia(String value) {
                this.provincia = value;
            }

            /**
             * Recupera il valore della proprietÓ nazione.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNazione() {
                return nazione;
            }

            /**
             * Imposta il valore della proprietÓ nazione.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNazione(String value) {
                this.nazione = value;
            }

        }

    }

}
