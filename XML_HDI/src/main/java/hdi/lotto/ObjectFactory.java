//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2019.02.01 alle 06:01:33 PM CET 
//


package hdi.lotto;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the hdi.lotto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: hdi.lotto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Lotto }
     * 
     */
    public Lotto createLotto() {
        return new Lotto();
    }

    /**
     * Create an instance of {@link Lotto.Documento }
     * 
     */
    public Lotto.Documento createLottoDocumento() {
        return new Lotto.Documento();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto }
     * 
     */
    public Lotto.Documento.Contratto createLottoDocumentoContratto() {
        return new Lotto.Documento.Contratto();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Beni }
     * 
     */
    public Lotto.Documento.Contratto.Beni createLottoDocumentoContrattoBeni() {
        return new Lotto.Documento.Contratto.Beni();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Beni.Autovettura }
     * 
     */
    public Lotto.Documento.Contratto.Beni.Autovettura createLottoDocumentoContrattoBeniAutovettura() {
        return new Lotto.Documento.Contratto.Beni.Autovettura();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Beni.Autovettura.Rischi }
     * 
     */
    public Lotto.Documento.Contratto.Beni.Autovettura.Rischi createLottoDocumentoContrattoBeniAutovetturaRischi() {
        return new Lotto.Documento.Contratto.Beni.Autovettura.Rischi();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.ProprietarioVeicolo }
     * 
     */
    public Lotto.Documento.Contratto.ProprietarioVeicolo createLottoDocumentoContrattoProprietarioVeicolo() {
        return new Lotto.Documento.Contratto.ProprietarioVeicolo();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto }
     * 
     */
    public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto createLottoDocumentoContrattoProprietarioVeicoloSoggetto() {
        return new Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza }
     * 
     */
    public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza createLottoDocumentoContrattoProprietarioVeicoloSoggettoResidenza() {
        return new Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Contraente }
     * 
     */
    public Lotto.Documento.Contratto.Contraente createLottoDocumentoContrattoContraente() {
        return new Lotto.Documento.Contratto.Contraente();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Contraente.Soggetto }
     * 
     */
    public Lotto.Documento.Contratto.Contraente.Soggetto createLottoDocumentoContrattoContraenteSoggetto() {
        return new Lotto.Documento.Contratto.Contraente.Soggetto();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza }
     * 
     */
    public Lotto.Documento.Contratto.Contraente.Soggetto.Residenza createLottoDocumentoContrattoContraenteSoggettoResidenza() {
        return new Lotto.Documento.Contratto.Contraente.Soggetto.Residenza();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata }
     * 
     */
    public Lotto.Documento.Testata createLottoDocumentoTestata() {
        return new Lotto.Documento.Testata();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Destinatari }
     * 
     */
    public Lotto.Documento.Testata.Destinatari createLottoDocumentoTestataDestinatari() {
        return new Lotto.Documento.Testata.Destinatari();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Destinatari.Soggetto }
     * 
     */
    public Lotto.Documento.Testata.Destinatari.Soggetto createLottoDocumentoTestataDestinatariSoggetto() {
        return new Lotto.Documento.Testata.Destinatari.Soggetto();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito }
     * 
     */
    public Lotto.Documento.Testata.Destinatari.Soggetto.Recapito createLottoDocumentoTestataDestinatariSoggettoRecapito() {
        return new Lotto.Documento.Testata.Destinatari.Soggetto.Recapito();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Servizio }
     * 
     */
    public Lotto.Documento.Testata.Servizio createLottoDocumentoTestataServizio() {
        return new Lotto.Documento.Testata.Servizio();
    }

    /**
     * Create an instance of {@link Lotto.Servizio }
     * 
     */
    public Lotto.Servizio createLottoServizio() {
        return new Lotto.Servizio();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Causale }
     * 
     */
    public Lotto.Documento.Contratto.Causale createLottoDocumentoContrattoCausale() {
        return new Lotto.Documento.Contratto.Causale();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.PuntoVendita }
     * 
     */
    public Lotto.Documento.Contratto.PuntoVendita createLottoDocumentoContrattoPuntoVendita() {
        return new Lotto.Documento.Contratto.PuntoVendita();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio }
     * 
     */
    public Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio createLottoDocumentoContrattoBeniAutovetturaRischiRischio() {
        return new Lotto.Documento.Contratto.Beni.Autovettura.Rischi.Rischio();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione }
     * 
     */
    public Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione createLottoDocumentoContrattoProprietarioVeicoloSoggettoResidenzaNazione() {
        return new Lotto.Documento.Contratto.ProprietarioVeicolo.Soggetto.Residenza.Nazione();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione }
     * 
     */
    public Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione createLottoDocumentoContrattoContraenteSoggettoResidenzaNazione() {
        return new Lotto.Documento.Contratto.Contraente.Soggetto.Residenza.Nazione();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio }
     * 
     */
    public Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio createLottoDocumentoTestataDestinatariSoggettoModalitaInvio() {
        return new Lotto.Documento.Testata.Destinatari.Soggetto.ModalitaInvio();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione }
     * 
     */
    public Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione createLottoDocumentoTestataDestinatariSoggettoRecapitoNazione() {
        return new Lotto.Documento.Testata.Destinatari.Soggetto.Recapito.Nazione();
    }

    /**
     * Create an instance of {@link Lotto.Documento.Testata.Servizio.Mittente }
     * 
     */
    public Lotto.Documento.Testata.Servizio.Mittente createLottoDocumentoTestataServizioMittente() {
        return new Lotto.Documento.Testata.Servizio.Mittente();
    }

    /**
     * Create an instance of {@link Lotto.Servizio.Mittente }
     * 
     */
    public Lotto.Servizio.Mittente createLottoServizioMittente() {
        return new Lotto.Servizio.Mittente();
    }

}
